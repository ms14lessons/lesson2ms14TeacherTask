package az.ingress.Lesson2ms14TeacherTask.service;

import az.ingress.Lesson2ms14TeacherTask.model.Teacher;
import az.ingress.Lesson2ms14TeacherTask.repository.TeacherRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {
    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }


    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public void update(Teacher teacher) {
        Optional<Teacher> teacherOptional = teacherRepository.findById(teacher.getId());
        teacherOptional.ifPresent(teacher1 -> {
            teacher1.setName(teacher.getName() != null ? teacher.getName() : teacher1.getName());
            teacher1.setFaculty(teacher.getFaculty() != null ? teacher.getFaculty() : teacher1.getFaculty());
            teacher1.setBirthdate(teacher.getBirthdate() != null ? teacher.getBirthdate() : teacher1.getBirthdate());
            teacherRepository.save(teacher1);
        });
    }

    public Teacher get(Integer id) {
        return teacherRepository.findById(id).get();
    }

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }

    public void delete(Integer id) {
        teacherRepository.deleteById(id);
    }


}
