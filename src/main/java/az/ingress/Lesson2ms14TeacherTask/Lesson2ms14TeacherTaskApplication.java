package az.ingress.Lesson2ms14TeacherTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson2ms14TeacherTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lesson2ms14TeacherTaskApplication.class, args);
	}

}
