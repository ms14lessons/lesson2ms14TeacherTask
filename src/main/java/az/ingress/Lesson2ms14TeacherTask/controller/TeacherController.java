package az.ingress.Lesson2ms14TeacherTask.controller;

import az.ingress.Lesson2ms14TeacherTask.model.Teacher;
import az.ingress.Lesson2ms14TeacherTask.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    public void create(@RequestBody Teacher teacher) {
        teacherService.create(teacher);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody Teacher teacher) {
        teacherService.update(teacher);
    }

    @GetMapping("/{id}")
    public Teacher get(@PathVariable Integer id) {
        return teacherService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        teacherService.delete(id);
    }



    @GetMapping
    public List<Teacher> getAll() {
        return teacherService.getAll();
    }

}
