package az.ingress.Lesson2ms14TeacherTask.repository;

import az.ingress.Lesson2ms14TeacherTask.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher,Integer> {

}
